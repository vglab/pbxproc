package main

type accountID = string
type sipUser = string
type uuID = string
type pstnNum = string

type model struct {
    accounts map[accountID]*account
    devices  map[sipUser]accountID
	numbers  map[pstnNum]accountID
    seNodes  map[uuID]*node
}

type account struct {
    ID      string
	Extensions map[string]*extension
		
}

type extension struct {
	Name    string
	Type    string
	PSTN    string
	Devices map[string]*device
}

type device struct {
    Name string
	User string
	Pass string
}

var m *model

func init() {
	m = &model{
		accounts: make(map[accountID]*account), 
		devices: make(map[sipUser]accountID), 
		numbers: make(map[pstnNum]accountID), 
		seNodes: make(map[uuID]*node),
	}
}

func (m *model) findAccountByID(id accountID) *account {
    return m.accounts[id]
}

func (m *model) findAccountExtenByDevice(name sipUser) (*account, *extension) {
	if id, ok := m.devices[name]; ok {
		if account, ok := m.accounts[id]; ok {
			for _, exten := range account.Extensions {
				for _, device := range exten.Devices {
					if device.Name == name {
						return account, exten
					}
				}
			}
		}
	}
	return nil, nil
}
func (m *model) findAccountExtenByNumber(num pstnNum) (*account, *extension) {
	if id, ok := m.numbers[num]; ok {
		if account, ok := m.accounts[id]; ok {
			for _, exten := range account.Extensions {
				if exten.PSTN == num {
					return account, exten
				}
			}
		}
	}
	return nil, nil
}

func (a *account) findExtension(name string) *extension {
	return a.Extensions[name]
}
