package main

import (
	api "bitbucket.org/vglab/fsesl/api"
)

func handleEventSE(n *node, e *api.Event) {
	switch e.Name {
	case api.EventNames.Incoming:
		account, exten := m.findAccountExtenByDevice(e.FmUser)
		if account == nil {
			// The call is either from a PSTN number or an unrecognized device.
			// First check to see if the target is a recognized PSTN number. If so, it's a call from the PSTN (and not a registered device).
			account, exten = m.findAccountExtenByNumber(e.ToUser)
		}
		if account == nil {
			// We don't recognize the FmUser as a valid device, and the ToUser is not a mapped PSTN number. Done.
			// Send reject
			return
		}
		xx := newNode(nil, exten.Type)
		xx.account   = account
		xx.extension = exten
		xx.events <-e

	case api.EventNames.Closed:
		for _, node := range n.peers {
			node.events <-e
		}
	}
}

func handleCmdSE(n *node, c api.Command) {
	
}

func extractAccountID(e *api.Event) string {
	//e.ToHost, e.FmHost
	return ""
}