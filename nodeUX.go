package main

import "bitbucket.org/vglab/fsesl/api"


func handleEventUX(n *node, e *api.Event) {
	switch e.Name {
	case api.EventNames.Incoming:
		// Have account and extension - but don't know whether to answer or dial out.
		// If the FmUser is a device on the extension, then dial out (A->B)
		// Otherwise, FmUser is an outside PSTN number, and so this UX must handle it (VM, fwd, etc)

		isFmDev := false
		for _, device := range n.extension.Devices {
			if device.Name == e.FmUser {
				isFmDev = true
				break
			}
		}
		if isFmDev {
			// This is A->B call - so where is B? Local extension? MS extension? PSTN?
			// Skip MS for now
			// Is the target a valid extension on this account?
			for _, toExt := range n.account.Extensions {
				if toExt.Name == e.ToUser {
					// Found the target as a valid extension on this account.
					// Tell that extension that it has an incoming call.
					xx := newNode(n, toExt.Type)
					xx.account   = n.account
					xx.extension = toExt
					xx.events <-e
					return
				}
			}
			// If we get here, we don't recognize the target as an extension. Must be PSTN.
			xx := newNode(n, NodeTypes.SE)
			xx.account   = n.account
			xx.extension = n.extension
			//xx.commands <- &api.Call{} // TODO: fill in
			return
		}
	}
}

func handleCmdUX(n *node, c api.Command) {
	
}

