package main

import (
	"fmt"
	"strings"

	api "bitbucket.org/vglab/fsesl/api"
)

func main() {
	
	fmt.Println("Main starting")
	apiC := api.NewClient()
	apiC.Connect("127.0.0.1", 0, "")

	logInit(apiC)

	for {
		select {
		case apiE := <-apiC.Events:
			logEvent(apiC, apiE)

			switch apiE.Name {
			case api.EventNames.Incoming:
				if _, ok := m.seNodes[apiE.UUID]; !ok {
					// Good. Got incoming event with a UUID we don't recognize. As it should be.
					n := newNode(nil, NodeTypes.SE)
					n.data["apic"] = apiC
					m.seNodes[apiE.UUID] = n
					n.events <-apiE
				}

			case api.EventNames.Closed:
				if node, ok := m.seNodes[apiE.UUID]; ok {
					delete(m.seNodes, apiE.UUID)
					node.events <-apiE
				}

			default:
				if n, ok := m.seNodes[apiE.UUID]; ok {
					n.events <-apiE
				} else {
					// Got an event other than Incoming with an unrecognized UUID. Probably not good.
				}
			}
		}
	}
}

func firstSegment(val, sep string) string {
	segs := strings.Split(val, sep)
	if len(segs) > 0 {
		return segs[0]
	}
	return val
}

var logRowCnt = 10

func logInit(apiC *api.Client) {
	apiC.LogAddField(api.Vars.Name, 			"Event",		15, "")
	apiC.LogAddField(api.Vars.UUID, 			"UUID",			10, "-")
	apiC.LogAddField(api.Vars.Direction, 		"Direction",	10, "")
	apiC.LogAddField(api.Vars.State, 			"Ans-State",	10, "")
	apiC.LogAddField(api.Vars.FmUser, 			"FmUser",		25, "")
	apiC.LogAddField(api.Vars.FmDisplay, 		"FmDisplay",	10, "")
	apiC.LogAddField(api.Vars.FmHost, 			"FmHost",		40, "")
	apiC.LogAddField(api.Vars.ToUser, 			"ToUser",		25, "")
	apiC.LogAddField(api.Vars.ToHost, 			"ToHost",		40, "")
	apiC.LogAddField(api.Vars.UserAgent, 		"UserAgent", 	25, "")
	apiC.LogAddField(api.Vars.HangupCause, 		"HUPCause",		20, "")
	apiC.LogAddField(api.Vars.HangupDisp, 		"HUPDisp",		15, "")
	apiC.LogAddField(api.Vars.UUIDA, 			"UUIDA",		10, "-")
	apiC.LogAddField(api.Vars.UUIDB, 			"UUIDB",		10, "-")
}
func logEvent(apiC *api.Client, apiE *api.Event) {
	logRowCnt++
	if logRowCnt >= 10 {
		logRowCnt = 0
		fmt.Println(" ")
		fmt.Println(apiC.LogHeaders())
	}
	fmt.Println(apiC.LogRow(apiE))
}