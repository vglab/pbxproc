package main

import api "bitbucket.org/vglab/fsesl/api"

type nodetype = string
var NodeTypes = struct {
	SE nodetype   // SIP endpoint
	UX nodetype   // Unlimited extension
	MX nodetype   // Metered extension
	VR nodetype   // Virtual receptionist
	CG nodetype   // Call group
	CQ nodetype   // Call queue
	FM nodetype   // Follow-me
	VM nodetype   // Voicemail
}{ "SE", "UX", "MX", "VR", "CG", "CQ", "FM", "VM"}

type node struct {
	nodeType    nodetype
	peers       []*node
	account     *account
	extension   *extension
	data        map[string]interface{}
	commands	chan api.Command
	events      chan *api.Event
	handleEvent func(*node, *api.Event)
	handleCmd   func(*node, api.Command)
}

func (n *node) run() {
	for {
		select {
		case event := <-n.events:
			n.handleEvent(n, event)
		case command := <-n.commands:
			n.handleCmd(n, command)
		}
	}
}

func newNode(parent *node, nodeType nodetype) *node {
	nn := &node{
		nodeType:  nodeType,
		peers:     []*node{},
		account:   &account{},
		extension: &extension{},
		data:      make(map[string]interface{}),
		commands:  make(chan api.Command),
		events:    make(chan *api.Event),
	}
	if parent != nil {
		nn.peers = append(nn.peers, parent)
	}

	switch nodeType {
	case NodeTypes.CG:
		nn.handleEvent = handleEventCG
		nn.handleCmd   = handleCmdCG
	case NodeTypes.CQ:
		nn.handleEvent = handleEventCQ
		nn.handleCmd   = handleCmdCQ
	case NodeTypes.FM:
		nn.handleEvent = handleEventFM
		nn.handleCmd   = handleCmdFM
	case NodeTypes.MX:
		nn.handleEvent = handleEventMX
		nn.handleCmd   = handleCmdMX
	case NodeTypes.SE:
		nn.handleEvent = handleEventSE
		nn.handleCmd   = handleCmdSE
	case NodeTypes.UX:
		nn.handleEvent = handleEventUX
		nn.handleCmd   = handleCmdUX
	case NodeTypes.VM:
		nn.handleEvent = handleEventVM
		nn.handleCmd   = handleCmdVM
	case NodeTypes.VR:
		nn.handleEvent = handleEventVR
		nn.handleCmd   = handleCmdVR
	default:
		return nil
	}
	return nn
}