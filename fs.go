package main

import (
	"bufio"
	"fmt"
	"net"
)

func freeswitch() error {
	fmt.Println("freeswitch(): starting")
	if lis, err := net.Listen("tcp", "127.0.0.1:8021"); err != nil {
		fmt.Printf("freeswitch(): got error listening on TCP: %s\n", err.Error())
		return err
	} else {
		fmt.Printf("freeswitch(): listen succeeded, accepting")
		go func() {
			conn, err := lis.Accept()
			if err != nil {
				fmt.Printf("freeswitch(): got error accepting on TCP: %s\n", err.Error())
				return
			}
			lr := bufio.NewReader(conn)
			for {
				if line, _, err := lr.ReadLine(); err == nil {
					lineStr := string(line)
					fmt.Printf("FS:-> %s\n", lineStr)
				} else {
					fmt.Printf("FS-> (got error: %s)", err)
				}
			}
		}()
	}
	fmt.Println("freeswitch(): returning (its running)")
	return nil
}